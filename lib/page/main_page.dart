import 'package:flutter/material.dart';
import 'package:flutter_xinghai/page/main_pages/page_0_find.dart';
import 'package:flutter_xinghai/page/main_pages/page_1_class.dart';
import 'package:flutter_xinghai/page/main_pages/page_2_recommed.dart';
import 'package:flutter_xinghai/page/main_pages/page_3_my.dart';

/**
 * @author lizulin
 * @since 2019/3/8
 * 主页面，主要用于自定义底部导航栏和导航页面切换
 */

class MainPage extends StatefulWidget {
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin {
  /// 底部导航栏控制器
  TabController _tabController;

  /// 底部导航栏标号，初始值为0，最大值为（导航页面数量 - 1）
  int _index = 0;

  /// 底部导航栏页面
  List<Widget> _pages;

  @override
  void initState() {
    super.initState();
    _pages = [
      PageFind(), //底部导航响应页面：发现页
      PageClass(),  //底部导航响应页面：分类页
      PageRecommend(),  //底部导航响应页面：推荐页
      PageMy(), //底部导航响应页面：个人页
    ];
    _tabController = TabController(
      vsync: this, //动画效果的异步处理，与SingleTickerProviderStateMixin配合使用
      initialIndex: _index, //初始导航页面标号，从0到length
      length: _pages.length //导航页面数量
    );
    _tabController.addListener(() { //添加监听器，控制页面切换
      if (_index != _tabController.index)
        setState(() {
          _index = _tabController.index;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //底部导航栏[发现-分类-推荐-个人]
      bottomNavigationBar: Container(
        padding: const EdgeInsets.all(4.0),
        height: 36.0,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.blueGrey,
              Color.fromRGBO(58,95,131,1),
            ]
          ),
        ),
        child: TabBar(
          isScrollable: false,
          controller: _tabController,
          indicatorWeight:1.0,
          indicatorColor: Colors.transparent, //修改指示器颜色为透明
          // 自定义导航栏
          tabs: <Widget>[
            Container(
                child: _index == 0
                    ? Image.asset('assets/images/tabs/actionTab0.png',color: Colors.white,)
                    : Image.asset('assets/images/tabs/tab0.png',color: Colors.white,),
            ),
            Container(
                child: _index == 1
                    ? Image.asset('assets/images/tabs/actionTab1.png',color: Colors.white,)
                    : Image.asset('assets/images/tabs/tab1.png',color: Colors.white,),
            ),
            Container(
                child: _index == 2
                    ? Image.asset('assets/images/tabs/actionTab2.png',color: Colors.white,)
                    : Image.asset('assets/images/tabs/tab2.png',color: Colors.white,),
            ),
            Container(
                child: _index == 3
                    ? Image.asset('assets/images/tabs/actionTab3.png',color: Colors.white,)
                    : Image.asset('assets/images/tabs/tab3.png',color: Colors.white,),
            ), 
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        physics: NeverScrollableScrollPhysics(), //禁用滑动
        children: _pages,
      ),
    );
  }
}

