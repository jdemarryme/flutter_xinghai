import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widget_util.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/model/book_model.dart';


class Page17KMoreNew extends StatelessWidget {

  Page17KMoreNew({
    @required this.title,
    @required this.bookModelList
  });

  final String title;
  final List<List<BookModel>> bookModelList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: 920.0,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 0.0,
                child: Container(
                  height: 950.0,
                  width: 550,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blueGrey[300],
                        Color.fromRGBO(58,95,131,1),
                      ]
                    ),
                  ),
                )
              ),
              PageView(
                children: _getBookGridList(context, bookModelList),
              ),
              Positioned(
                top: 30.0,
                left: 0.0,
                child: IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.white,),
                  onPressed:  () => Navigator.of(context).pop(),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 40.0),
                  child: Text(title,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


List<Widget> _getBookGridList(BuildContext context, List<List<BookModel>> bookModelList) {
  return List.generate(bookModelList.length, (index) {
    return Container(
      height: 800.0,
      width: 400.0,
      child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: _getContainerItemList(context,bookModelList[index]),
            ),
            Text('${index+1}/${bookModelList.length}', 
              style: TextStyle(fontSize: 16.0, color: Colors.white, fontWeight: FontWeight.bold),),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
    );
  });
}

List<Widget> _getContainerItemList(BuildContext context, List<BookModel> bookModelList) {
  return List.generate(bookModelList.length, (index) {
    BookModel bookModel = bookModelList[index];
    if (index == 0) {
      return Container(
        padding: EdgeInsets.only(top: 75.0, bottom: 15),
        height: 340.0,
        width: double.infinity,
        child: 
        Container(
          color: Colors.white12,
          height: 210.0,
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              MyCachedBookImage(
                bookIndex: bookModel.bookIndex,
                bookSourceId: bookModel.sourceId,
                bookImgUrl: bookModel.imageUrl, 
                height: 210.0, width: 150.0
              ),
              SizedBox(width: 20.0,),
              Container(
                height: 210.0,
                width: 180.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(bookModel.title, style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold),),
                    Text(bookModel.value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),),
                    Text('    ${bookModel.description}', style: TextStyle(fontSize: 18.0),),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Container(
      height: 180.0,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 210.0,
            height: 140.0,
            color: Colors.white12,
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(bookModel.title, style: TextStyle(fontSize: 19.0, 
                  fontWeight: FontWeight.bold), maxLines: 1,overflow: TextOverflow.ellipsis,),
                Text(bookModel.value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),),
                Text('    ${bookModel.description}',
                  style: TextStyle(fontSize: 17.0),maxLines: 3,overflow: TextOverflow.ellipsis,),
              ],
            ),
          ),
          MyCachedBookImage(
            bookIndex: bookModel.bookIndex,
            bookSourceId: bookModel.sourceId,
            bookImgUrl: bookModel.imageUrl, 
          ),
        ],
      ),
    );
  });
}