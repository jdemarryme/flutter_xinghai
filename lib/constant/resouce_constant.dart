class ResourceConstant {
  /// 图片：书法字体“更多”
  static final String imageMore = 'assets/images/more.png';
  /// 图片：书法字体“推荐作品”
  static final String imageProductLike = 'assets/images/product_like.png';
  /// 字体：“推荐作品”
  static final String textProductLike = '推荐作品';

  /// 图片：书法字体“最新作品”
  static final String imageProductNew = 'assets/images/product_new.png';
  /// 字体：“最新作品”
  static final String textProductNew = '最新作品';

  /// 图片：书法字体“热门作品”
  static final String imageProductHot = 'assets/images/product_hot.png';
  /// 字体：“热门作品”
  static final String textProductHot = '热门作品';
}