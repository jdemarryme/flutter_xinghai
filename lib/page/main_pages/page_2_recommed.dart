import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/data_util.dart';
import 'package:flutter_xinghai/common/pages/my_error_page.dart';
import 'package:flutter_xinghai/common/pages/my_waiting_page.dart';
import 'package:flutter_xinghai/common/widget_util.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'package:flutter_xinghai/model/json_and_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_book/page_book_item.dart';

class PageRecommend extends StatefulWidget {
  @override
  _PageRecommendState createState() => _PageRecommendState();
}

class _PageRecommendState extends State<PageRecommend> {

  void initState() { 
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    return FutureBuilder(
      future: myFetchData(
        Constant.page2RecommendKey, //缓存数据的key
        Constant.page2TimeKey,  //缓存数据的时间key
        Constant.timeExpire1Day,  //缓存数据的缓存时间
        '${Constant.hostUrl}/api/util/get17KRecommendList'  //请求网络数据地址
      ),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MyWaitingPage();
        }
        JsonAndModel jsonAndModel = snapshot.data;
        switch (jsonAndModel.status) {
          case Constant.statusCodeOK:
            return _myRecommendPage(context, jsonAndModel.data);
          case Constant.statusNoFound:
            return MyErrorPage(title: '获取数据失败',info: '${jsonAndModel.msg}',method: () {setState(() {});},);
          default:
            return MyErrorPage(title: '服务器出错',info: '错误原因：${jsonAndModel.msg}', method: () {setState(() {});},);
        }
      },
    );
  }
}

Widget _myRecommendPage(BuildContext context, String data) {
  //将JSONArray数组解析成model列表
  List response = json.decode(data);
  List<BookModel> dataList = 
    response.map((m) => BookModel.fromJson(m)).toList();
  return Scaffold(
    body: Container(
      // padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(58,95,131,1),
            Colors.blueGrey,
          ]
        )
      ),
      child: ListView(
        children: List.generate(dataList.length, (index) {
          BookModel data = dataList[index];
            return Column(
              children: <Widget>[
                index == 0
                ? Container(
                height: 54.0,
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Image.asset('assets/images/recommend.png', color: Colors.white,),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.blueGrey,
                      Color.fromRGBO(58,95,131,1),
                    ]
                  )
                ),
              )
              : SizedBox(),
          Container(
            padding: EdgeInsets.all(8.0),
            child: Card(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 16/9,
                    child: myCachedPressedImage(data.imageUrl,() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => PageBookItem(bookIndex: data.bookIndex, bookSourceId: int.parse(data.sourceId),)
                        )
                      );
                    }),
                  ),
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                        data.value
                      ),
                    ),
                    title: Text(data.title),
                    subtitle: Text('作者：${data.type}'),
                  ),
                  Container(
                    padding: EdgeInsets.all(16.0),
                    child: Text(data.description,
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  ButtonTheme.bar(
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: Text('喜欢',),
                          onPressed: () {},
                        ),
                        FlatButton(
                          child: Text('关注',),
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
              index == dataList.length - 1
              ? Container(
                height: 40.0,
                child: Center(
                  child: Text('一不小心就到底儿了！',style: TextStyle(color: Colors.white),)
                ),
              )
              : SizedBox()
              ],
            );
          }
        ).toList(),
      ),
    ),
  );
}

/**
 * 缓存可点击图片
 * @param imageUrl 图片URL链接
 * @param onTapMethod 点击图片触发事件
 */
Widget myCachedPressedImage(String imageUrl, VoidCallback onTapMethod) {
  return Container(
    child: Stack(
      children: <Widget>[
        CachedNetworkImage(
          imageUrl: imageUrl,
          placeholder: (BuildContext context, url) => CircularProgressIndicator(),
          errorWidget: (BuildContext context, url, error) => Icon(Icons.error),
          fit: BoxFit.cover,
        ),
        Positioned.fill(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              splashColor: Colors.white.withOpacity(0.5),
              highlightColor: Colors.white.withOpacity(0.1),
              onTap: onTapMethod,
            ),
          ),
        ),
      ],
    ),
  );
}