import 'package:flutter/material.dart';

class PageNoDevelopedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.blueGrey,
              Color.fromRGBO(58,95,131,1),
            ]
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 100.0,),
            Container(
              height: 240.0,
              width: double.infinity,
              margin: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                color: Colors.white12,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: 64.0,
                    width: double.infinity,
                    child: Center(
                      child: Icon(
                        Icons.warning, 
                        color: Colors.white54,
                        size: 54.0,
                      ),
                    )
                  ),
                  Text('建设中', 
                    style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  Text('我们正在努力建设中...',
                    style: TextStyle(
                      color: Colors.white70,
                      fontSize: 20.0
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}