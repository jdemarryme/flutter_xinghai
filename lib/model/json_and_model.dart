import 'package:json_annotation/json_annotation.dart';
part 'json_and_model.g.dart';

@JsonSerializable()
class JsonAndModel {
  int status;
  String msg;
  dynamic data;

  JsonAndModel({
    this.status,
    this.msg,
    this.data
  });

  factory JsonAndModel.fromJson(Map<String, dynamic> json) => _$JsonAndModelFromJson(json);
  Map<String, dynamic> toJson() => _$JsonAndModelToJson(this);
}
