import 'package:flutter/material.dart';

class PageMyAttention extends StatefulWidget {
  @override
  _PageMyAttentionState createState() => _PageMyAttentionState();
}

class _PageMyAttentionState extends State<PageMyAttention> {
  @override
  Widget build(BuildContext context) {
  return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(58,95,131,1),
        title: Text('免责申明',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      backgroundColor: Color.fromRGBO(58,95,131,1),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              '''    星海阅读软件，仅仅提供学习参考，不提供商业服务。所有素材来自17K小说网（http://www.17k.com/）、爱设计网（http://www.asj.com.cn/）等网站，如有侵权请和我们联系，联系方式为app_xinghai@163.com
              ''', style: TextStyle(fontSize: 22.0, color: Colors.white),
            ),
            Text('2019年3月15日',
              style: TextStyle(fontSize: 20.0, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}