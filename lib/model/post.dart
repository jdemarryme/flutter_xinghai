class Post {
  Post({
    this.imageUrl,
    this.title,
    this.author,
    this.description
  });

  final String imageUrl;
  final String title;
  final String author;
  final String description;
}

final List<Post> list1 = [
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/14546/72/2909235-1539919872000.jpg',
    author: '作者：刑木',
    description: '推动死亡的手来自每一个污秽的人，罪孽都应受到制裁。我！刑警！缉凶！',
    title: '缉凶',
  ),
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/14654/73/2930948-1545271710000.jpg',
    author: '作者：河中',
    description: '我辗转于纸醉金迷的夜场，形形色色的美女之中，直到她的出现……',
    title: '迷失的青春',
  ),
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/14654/73/2930950-1545979358000.jpg',
    author: '作者：伊昂扬',
    description: '修真界帝尊玩手机拍电影，地球大巨头要炼丹要修道，离了小爷玩不转！',
    title: '跨界修真大富豪',
  ),
];

final List<Post> list2 = [
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/14162/70/2832479-1530868517000.jpg',
    author: '作者：乔安乐',
    description: '她助他登基，以求皇后之位。婚后，他从不碰她，并迎娶了她的额妹妹……',
    title: '薛皇后',
  ),
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2019/14646/73/2929231-1547098419000.jpg',
    author: '作者：洛花央',
    description: '司琴最近还有一个新的爱好，那就是穿着清凉的出现在某人的面前。',
    title: '娇妻撩人：柯家二少很害羞',
  ),
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/14425/72/2885102-1542252154000.jpg',
    author: '作者：珞十七',
    description: '昔日的药神谷谷主，闻名大陆的鬼医至尊，竟被自己最得意的徒弟害死！',
    title: '狼系狂妻：冷帝狠狠宠',
  ),
];

final List<Post> list3 = [
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2013/2466/12/493239-1364709925000_1.jpg',
    author: '3086771',
    description: '论潜力，不算天才，可玄功武技，皆可无师自通。 论实力，任凭你有万千至宝，但定不敌我界灵大军。 我是谁？天下众生视...',
    title: '修罗武神',
  ),
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/14585/72/2917108-1541675255000_1.jpg',
    author: '697',
    description: '我本是一个普普通通的学生党，结果意外遇到一个神秘美女姐姐，她改变了我的一生。什么？我变成了僵尸？你要让我拯救这个世界？...',
    title: '僵尸在校园',
  ),
  Post(
    imageUrl: 'http://img.17k.com/images/bookcover/2018/13434/67/2686894-1530181548000_1.jpg',
    author: '1580',
    description: '“你愿意跟我结婚吗？就现在。” “可是我很穷，我还小，我还在上学。” “没关系，只要是你就行了。” 一个是荒唐无稽...',
    title: '权门婚宠',
  ),
];