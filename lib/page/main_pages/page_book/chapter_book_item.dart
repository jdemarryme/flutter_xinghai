import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/pages/my_error_page.dart';
import 'package:flutter_xinghai/common/pages/my_waiting_page.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/entity/book.dart';
import 'package:flutter_xinghai/model/json_and_model.dart';
import 'package:flutter_xinghai/sqflite/book_database.dart';
import 'package:http/http.dart' as http;

class ChapterBookItem extends StatefulWidget {

  final String bookId;
  final String id;
  final String indexId;

  ChapterBookItem({this.bookId, this.id,this.indexId});

  @override
  _ChapterBookItemState createState() => _ChapterBookItemState(
    id: id, bookId: bookId, indexId: indexId
  );
}

class _ChapterBookItemState extends State<ChapterBookItem> {

  String id;
  String bookId;
  String indexId;
  _ChapterBookItemState({this.id, this.bookId, this.indexId});

  /// 返回的章节内容
  String chapter;
  /// 查询数据库返回的
  Book book;
  /// 数据库中图书的详细内容
  Map chapterMap;

  @override
  void initState() {
    super.initState();
  }

  Future<JsonAndModel> _fetchChapterData() async {
    final BookDataBase bookDataBase = BookDataBase();
    book = await bookDataBase.getBookById(bookId);
    if (book == null) {
      return JsonAndModel(
        status: Constant.statusNoFound, msg: Constant.msgNoFound, data: null);
    } else {
      String mapString = book.freeListContent;
      if (mapString.isEmpty || mapString == null) {
        chapterMap = Map();
      } else {
        chapterMap = json.decode(mapString); //将转义的字符串解析出来
        if (chapterMap.containsKey(indexId)) {
          return JsonAndModel(
            status: Constant.statusCodeOK, msg: Constant.msgCodeOK, data: chapterMap[indexId]);
        }
      }
    }
    http.Response response;
    try {
      String url =
        '${Constant.getChapterURL}?id=$id&bookId=$bookId&indexId=$indexId';
      response =
          await http.get(url).timeout(Duration(seconds: 7)); //设置响应时间不超过5秒
    } catch (e) {
      return JsonAndModel(
          status: Constant.statusNoFound, msg: Constant.msgNoFound, data: null);
    }
    JsonAndModel jsonAndModel =
      JsonAndModel.fromJson(json.decode(response.body));
    if (jsonAndModel.status == Constant.statusCodeOK) {
      chapter = jsonAndModel.data;
      chapterMap[indexId] = chapter;  //存成map形式
      String string = json.encode(chapterMap); //将map编码并转义
      bookDataBase.updateChapterById(bookId, string);
      return JsonAndModel(
        status: Constant.statusCodeOK,
        msg: Constant.msgCodeOK,
        data: chapter);
    }
    return jsonAndModel;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fetchChapterData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MyWaitingPage();
        }
        JsonAndModel jsonAndModel = snapshot.data;
        switch (jsonAndModel.status) {
          case Constant.statusCodeOK:
            return MyChpaterPage(book: book, chapter:chapter, indexId: indexId,
              increase: () {
                setState(() {
                  indexId = '${int.parse(indexId)+1}';
                });
              },
              decrease: () {
                setState(() {
                  indexId = '${int.parse(indexId)-1}';
                });
              },
            );
          case Constant.statusNoFound:
            return MyErrorPage(
              title: '获取数据失败',
              info: '${jsonAndModel.msg}',
              method: () {
                setState(() {});
              },
              popContext: true,
            );
          default:
            return MyErrorPage(
              title: '服务器出错',
              info: '错误原因：${jsonAndModel.msg}',
              method: () {
                setState(() {});
              },
              popContext: true,
            );
        }
      },
    );
  }
}

class MyChpaterPage extends StatefulWidget {

  final Book book;
  final String chapter;
  final String indexId;
  final VoidCallback increase;
  final VoidCallback decrease;
  MyChpaterPage({this.book, this.chapter, this.indexId, this.increase, this.decrease});

  @override
  _MyChpaterPageState createState() => _MyChpaterPageState();
}

class _MyChpaterPageState extends State<MyChpaterPage> {
  bool onPressed = false;
  int _colorIndex = 0;
  List<Color> fontColorList = [
    Color.fromRGBO(255, 246, 230, 1),
    Color.fromRGBO(213, 206, 205, 1),
    Color.fromRGBO(73, 76, 73, 1),
    Colors.black,
    Color.fromRGBO(181, 137, 49, 1)
  ];
  List<Color> bgmColorList = [
    Color.fromRGBO(65, 80, 90, 1),
    Color.fromRGBO(65, 68, 65, 1),
    Color.fromRGBO(213, 198, 172, 1),
    Colors.white,
    Color.fromRGBO(8, 16, 16, 1),
  ];
  List<String> nameColorList = [
    '蓝','灰','黄','白','褐'
  ];
  double myfontSize = 18.0;
  
  

  @override
  Widget build(BuildContext context) {
    List<String> freeList = widget.book.freeList.split("\$");
    String title = freeList[int.parse(widget.indexId)];
    int index = int.parse(widget.indexId);
    return Scaffold(
      backgroundColor: bgmColorList[_colorIndex],
      body: Container(
        padding: EdgeInsets.all(4.0),
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                //标题
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        '${widget.book.bookName}',
                        style: TextStyle(
                          fontSize: 22.0,
                          color: fontColorList[_colorIndex]
                        ),
                      ),
                      Text(title,
                        style: TextStyle(
                          fontSize: 18.0,
                          color: fontColorList[_colorIndex],
                        ),
                      ),
                    ],
                  ),
                ),
                Text('${widget.chapter}',
                  style: TextStyle(
                    fontSize: myfontSize,
                    color: fontColorList[_colorIndex],
                    wordSpacing: 6.0,
                  ),
                ),
                (index <= 1)
                ? SizedBox(height: 1.0,width: 1.0,)
                : Container(
                  color: Colors.white70,
                  margin: EdgeInsets.all(8.0),
                  height: 50.0,
                  child: FlatButton(
                    child: Text('上一章： ${freeList[index-1]}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22.0
                      ),
                    ),
                    onPressed: widget.decrease,
                  )
                ),
                (index >= freeList.length)
                ? Container(
                  height: 30.0,
                  margin: EdgeInsets.all(8.0),
                  color: Colors.white70,
                  alignment: Alignment.bottomCenter,
                  child: Text('已经没有更多内容了！',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0
                    ),
                  ),
                )
                : Container(
                  height: 50.0,
                  color: Colors.white70,
                  margin: EdgeInsets.all(8.0),
                  child: FlatButton(
                    child: Text('下一章： ${freeList[index+1]}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22.0
                      ),
                    ),
                    onPressed: widget.increase,
                  )
                )
              ],
            ),
            Positioned(
              top: 25.0,
              right: 0.0,
              child: Container(
                color: Colors.black26,
                child: IconButton(
                  icon: Icon(Icons.menu, size: 28.0,color: Colors.white),
                  onPressed: () {
                    setState(() {
                     onPressed = !onPressed; 
                    });
                  },
                )
              ),
            ),
            onPressed
            ? Positioned(
                top: 25.0,
                left: 0.0,
                child: Container(
                  color: Colors.black26,
                  child: IconButton(
                    icon: Icon(Icons.arrow_back_ios, size: 28.0,color: Colors.white),
                    onPressed: () => Navigator.of(context).pop(),
                  )
                ),
              )
            : SizedBox(height: 1.0,width: 1.0,),
            onPressed
            ? Positioned(
              bottom: 10.0,
              left: 40.0,
              child: Container(
                width: 340.0,
                height: 130.0,
                padding: EdgeInsets.all(16.0),
                color: Colors.white70,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('主题颜色'),
                        SizedBox(width: 20.0,),
                        Row(
                          children: List.generate(fontColorList.length, (index) {
                            return CircleAvatar(
                              backgroundColor: bgmColorList[index],
                              child: FlatButton(
                                child: Text(nameColorList[index], style:TextStyle(
                                  color: fontColorList[index]
                                )),
                                onPressed: () {
                                   if (_colorIndex != index)
                                    setState(() {
                                     _colorIndex = index;  
                                    });
                                },
                              ),
                            );
                          }),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('字体大小'),
                        SizedBox(width: 20.0,),
                        IconButton(
                          icon: Icon(Icons.zoom_out),
                          onPressed: () {
                            setState(() {
                              if (myfontSize > 16.0) {
                                setState(() {
                                 myfontSize--; 
                                });
                              }
                            });
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.zoom_in),
                          onPressed: () {
                            setState(() {
                              if (myfontSize < 22.0) {
                                setState(() {
                                 myfontSize++; 
                                });
                              }
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
            : SizedBox(height:1.0,width:1.0),
          ], 
        ),
      )
    );
  }
}

