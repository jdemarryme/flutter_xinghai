import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widget_util.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

/**
 * @author lizulin
 * @since 2019/3/8
 * 登录页面，主要用于用户登录和注册
 */
class LoginPage extends StatefulWidget {
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        //渐变色背景
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color.fromRGBO(58,95,131,1),
                Color.fromRGBO(224 ,230,209,1),  
              ],
            )
          ),
          child: Column(
            children: <Widget>[
              SizedBox( height: 25.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios, color: Colors.white,),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  FlatButton(
                    child: Image.asset('assets/images/title.png', height: 30.0,color: Colors.white,),
                    onPressed: () {
                      //TODO:点击后打开星海阅读详情
                    },
                  ),
              ],),
              Image.asset('assets/images/logo.png', height: 170.0),
              SizedBox( height: 15.0,),
              Image.asset(
                'assets/images/welcome.png',
                color: Colors.white,
                height: 60.0,
              ),
              SizedBox(
                height: 25.0,
              ),
              //TabBarView for login and register
              Container(
                height: 500.0,
                child: DefaultTabController(
                  length: 2,
                  child: Scaffold(
                    backgroundColor: Colors.transparent,
                    appBar: TabBar(
                      indicatorColor: Colors.white,
                      indicatorSize: TabBarIndicatorSize.label,
                      tabs: <Widget>[
                        Tab(
                          child: Image.asset(
                            'assets/images/login.png',
                            height: 28.0,
                            color: Colors.white,
                          ),
                        ),
                        Tab(
                          child: Image.asset(
                            'assets/images/register.png',
                            height: 28.0,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    body: TabBarView(
                      children: <Widget>[
                        LoginForm(),  //登录页面
                        RegisterForm(), //注册页面
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final loginFormKey = GlobalKey<FormState>();
  String mail, pwd;

  /// 控制密码输入框的密码显示与隐藏
  bool _observeText = true;
  /// 进行自动验证
  bool _autoAvaildate = false;
  
  bool _isLogin = false;

  void submitLoginForm() async{
    //vlidate input data
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      var response;
      try {
        response = await http.post('${Constant.hostUrl}/api/mail/doLogin',body:
          {'mail':'$mail','pwd':'$pwd'}
        );
      } catch (exception) {
        response = null;
      }
      //vlidate request
      if (response != null && response.statusCode == 200) {
        final responseBody = json.decode(response.body);
        //validate mail and password
        int status = responseBody['status'];
        if (status == 200) {
          final String data = json.decode(responseBody['data']).toString();
          SharedPreferences preferences = await SharedPreferences.getInstance();
          preferences.setString(Constant.userKey, data);
          openAlertTipsDialog(context, Text('登录成功，欢迎使用星海阅读！'));
        } else {
          openAlertTipsDialog(context, Text('登录失败，${responseBody['msg']}！'));
        }
      } else {
        openAlertTipsDialog(context, Text('当前网络不可用，请确认网络状态！'));
      }
    } else {
      setState(() {
        _autoAvaildate = true;
        _isLogin = false;
      });
    }
  }

  String validateMail(value) {
    if (value.isEmpty) {
      return '邮箱不能为空!';
    } else if (!RegExp(r'\w+@[a-z0-9]+\.[a-z]{2,4}').hasMatch(value)) {
      return '邮箱账号格式不正确！';
    } else {
      return null;
    }
  }

  String validatePwd(value) {
    if (value.isEmpty) {
      return '密码不能为空';
    } else if (!RegExp(r'[\w_-]{6,16}').hasMatch(value)) {
      return '密码字母数字个数在6~16之间';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidate: _autoAvaildate,
      key: loginFormKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
            child: TextFormField(
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                icon: Icon(Icons.email),
                hintText: '邮箱账号',
              ),
              keyboardType: TextInputType.emailAddress,
              onSaved: (value) {
                mail = value;
              },
              validator: validateMail,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
            child: TextFormField(
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                icon: Icon(Icons.lock),
                hintText: '账号密码',
                suffixIcon: _observeText
                    ? IconButton(
                        icon: Icon(Icons.visibility),
                        onPressed: () {
                          setState(() {
                            _observeText = false;
                          });
                        },
                      )
                    : IconButton(
                        icon: Icon(Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _observeText = true;
                          });
                        },
                      ),
              ),
              obscureText: _observeText,
              onSaved: (value) {
                pwd = value;
              },
              validator: validatePwd,
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
              child: _isLogin
                  ? CircularProgressIndicator()
                  : RaisedButton(
                      color: Colors.blueGrey,
                      child: Text(
                        '登录',
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      ),
                      onPressed: submitLoginForm,
                      shape: StadiumBorder())),
        ],
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final registerFormKey = GlobalKey<FormState>();
  String mail, oldPwd, captcha, sendCaptcha;
  bool _isRegister = false;
  String _timeString = '获取验证码';
  int _timeCount = 59;
  Timer _timer; 


  String validateMail(value) {
    if (value.isEmpty) {
      return '邮箱不能为空!';
    } else if (!RegExp(r'\w+@[a-z0-9]+\.[a-z]{2,4}').hasMatch(value)) {
      return '邮箱账号格式不正确！';
    } else {
      return null;
    }
  }

  String validatePwd(value) {
    registerFormKey.currentState.save();
    if (oldPwd.isEmpty) {
      return '密码不能为空';
    } else if (!RegExp(r'[\w_-]{6,16}').hasMatch(oldPwd)) {
      return '密码字母数字个数在6~16之间';
    } else if (value == oldPwd) {
      return null;
    } else {
      return "两次输入密码不一致！";
    }
  }

  String validateCaptcha(value) {
    if (sendCaptcha == null) {
      return '还未发送邮箱验证！';
    } else if (value != sendCaptcha){
      return '输入验证码不正确！';
    } else {
      return null;
    }
  }

  _doRegister() async{
    if (registerFormKey.currentState.validate()) {
      registerFormKey.currentState.save();
      var response;
      try {
        response = await http.post('${Constant.hostUrl}/api/mail/doRegister',body:
          {'mail':'$mail','pwd':'$oldPwd'}
        );
      } catch (exception) {
        response = null;
      }
      //vlidate request
      if (response != null && response.statusCode == 200) {
        final responseBody = json.decode(response.body);
        //validate mail and password
        int status = responseBody['status'];
        if (status == 200) {
          final String data = json.decode(responseBody['data']).toString();
          SharedPreferences preferences = await SharedPreferences.getInstance();
          preferences.setString(Constant.userKey, data);
          openAlertTipsDialog(context, Text('注册成功，欢迎使用星海阅读！'));
        } else {
          openAlertTipsDialog(context, Text('注册失败，${responseBody['msg']}！'));
        }
      } else {
        openAlertTipsDialog(context, Text('当前网络不可用，请确认网络状态！'));
      }
    }
  }

  _getCaptcha() async {
    registerFormKey.currentState.save();
    //vlidate mail
    if (validateMail(mail) == null) {
      if(_timer != null) return;
      var response;
      try {
        response = await http.post('${Constant.hostUrl}/api/mail/sendMail',
        body: {'address':'$mail'}
        );
      } catch (exception) {
        response = null;
      }
      if (response != null && response.statusCode == 200) {
        final responseBody = json.decode(response.body);
        int status = responseBody['status'];
        if (status == 200) {
          sendCaptcha = responseBody['data'].toString();
          openAlertTipsDialog(context, Text('发送邮件成功！请前往邮箱查看'));
          setState(() {
           if(_timer != null) return;
           _timeString = '${_timeCount}s后重新获取';
           _timer =Timer.periodic(Duration(seconds: 1), (timer) {
             setState(() {
              _timeCount--;
              if (_timeCount > 0) {
                _timeString = '${_timeCount}秒';
              } else {
                _timeString = '获取验证码';
                _timeCount = 59;
                _timer.cancel();
                _timer =null;
              }
             });
           }); 
          });
        } else {
          openAlertTipsDialog(context, Text('发送邮件失败，${responseBody['msg']}！'));
        }
      }
    } else {
      openAlertTipsDialog(context, Text('邮箱格式不正确！'));
    }
  }

  @override
  void dispose() {
    super.dispose();
    _timer?.cancel();
    _timer = null;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: registerFormKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
            child: TextFormField(
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                icon: Icon(Icons.email),
                hintText: '注册邮箱',
              ),
              keyboardType: TextInputType.emailAddress,
              onSaved: (value) {
                mail = value;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextFormField(
                    cursorColor: Colors.white,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        icon: Icon(Icons.verified_user),
                        hintText: '输入验证码',
                        isDense: true),
                    keyboardType: TextInputType.number,
                    validator: validateCaptcha,
                  ),
                ),
                FlatButton(
                    child: Text(_timeString, style: TextStyle(fontSize: 18.0, color: Colors.black54),),
                    shape: StadiumBorder(side: BorderSide(color: Colors.black54)),
                    onPressed: _getCaptcha,
                  ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
            child: TextFormField(
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                icon: Icon(Icons.lock),
                hintText: '输入密码',
              ),
              keyboardType: TextInputType.text,
              obscureText: true,
              onSaved: (value) {
                oldPwd = value;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
            child: TextFormField(
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                icon: Icon(Icons.lock),
                hintText: '确认密码',
              ),
              keyboardType: TextInputType.text,
              validator: validatePwd,
              obscureText: true,
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 15.0),
              child: _isRegister
                  ? CircularProgressIndicator()
                  : RaisedButton(
                      color: Colors.blueGrey,
                      child: Text(
                        '注册',
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      ),
                      onPressed: _doRegister,
                      shape: StadiumBorder())),
        ],
      ),
    );
  }
}
