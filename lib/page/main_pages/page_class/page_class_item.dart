import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/data_util.dart';
import 'package:flutter_xinghai/common/pages/my_error_page.dart';
import 'package:flutter_xinghai/common/pages/my_waiting_page.dart';
import 'package:flutter_xinghai/common/widgets/my_blur_image_container.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'package:flutter_xinghai/model/json_and_model.dart';

class PageClassItem extends StatefulWidget {

  final int index;
  PageClassItem({this.index});

  @override
  _PageClassItemState createState() => _PageClassItemState(index:index);
}

class _PageClassItemState extends State<PageClassItem> {

  /// 分类标号
  int index;
  _PageClassItemState({this.index});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: myFetchData(
        '${Constant.page1ClassifyKey}$index',
        '${Constant.page1TimeKey}$index',
        Constant.timeExpire3Days,
        '${Constant.page1ClassURL}?index=$index'
      ),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MyWaitingPage();
        }
        JsonAndModel jsonAndModel = snapshot.data;
        switch (jsonAndModel.status) {
          case Constant.statusCodeOK:
            return MyClassifyPage(title: '${Constant.pageClassTitles[index]}',data: jsonAndModel.data.toString());
          case Constant.statusNoFound:
            return MyErrorPage(
              title: '获取数据失败', info: '${jsonAndModel.msg}',
              method: () {setState(() {});}, popContext: true,
            );
          default:
            return MyErrorPage(title: '服务器出错',info: '错误原因：${jsonAndModel.msg}', 
              method: () {setState(() {});}, popContext: true,);
        }
      },
    );
  }
}

class MyClassifyPage extends StatefulWidget {
  final String title;
  final String data;
  MyClassifyPage({this.title,this.data});
  @override
  _MyClassifyPageState createState() => _MyClassifyPageState(title: title, data: data);
}

class _MyClassifyPageState extends State<MyClassifyPage> {
  String title;
  String data;

  _MyClassifyPageState({this.title, this.data});

  @override
  Widget build(BuildContext context) {
    List response = json.decode(data);
    List<BookModel> dataList = response.map((m) 
      => BookModel.fromJson(m)).toList();
  return Scaffold(
    backgroundColor: Colors.blueGrey,
    body: Container(
      height: double.infinity,
      width: double.infinity,
      child: PageView(
          children: List.generate(dataList.length, (index) {
            BookModel bookModel = dataList[index];
            return Stack(
              children: <Widget>[
                MyBlurImageContainer(bookImageUrl: bookModel.imageUrl),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 85.0,),
                    MyCachedBookImage(
                      bookIndex: bookModel.bookIndex,
                      bookSourceId: bookModel.sourceId,
                      bookImgUrl: bookModel.imageUrl, 
                      height: 210.0, width: 150.0
                    ),
                    SizedBox(height: 15.0,),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                      ),
                      child: Column(
                        children: <Widget>[
                          Text(
                            bookModel.type, 
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0
                            )
                          ),
                          Text(bookModel.value,
                            style: TextStyle(
                              fontSize: 16.0, color: Colors.black54
                            ),
                          ),
                          Text('    ${bookModel.description}', 
                            maxLines: 8, 
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 16.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.all(10.0),
                    ),
                  ],
                ),
                Positioned(
                  top: 25.0,
                  left: 0.0,
                  child: Container(
                    color: Colors.white54,
                    child: Row(
                      children: <Widget>[
                        FlatButton.icon(
                          icon: Icon(Icons.arrow_back_ios),
                          label: Text('$title / ${bookModel.title}',
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 20.0
                            ),
                          ),
                          onPressed:  () => Navigator.of(context).pop(),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0.0,
                  right: 0.0,
                  child: Container(
                    color: Colors.white70,
                    child: Text('${index+1} / ${dataList.length}',
                      style: TextStyle(
                        color: Colors.black87
                      ),
                    )
                  ),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}


