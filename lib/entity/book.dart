import 'package:json_annotation/json_annotation.dart';
part 'book.g.dart';

@JsonSerializable()
class Book {
  int id;
  int bookId;
  String bookIndex;
  int bookSourceId;
  String bookImgUrl;
  String bookName;
  String bookInfo;
  int bookWordCount;
  String bookLastTime;
  int authorId;
  String authorIndex;
  String pullLastTime;
  String freeList;
  String vipList;
  String freeListContent;
  String vipListContent;
  int lastViewId;

  Book({
    this.id,
    this.bookId,
    this.bookIndex,
    this.bookSourceId,
    this.bookImgUrl,
    this.bookName,
    this.bookInfo,
    this.bookWordCount,
    this.bookLastTime,
    this.authorId,
    this.authorIndex,
    this.pullLastTime,
    this.freeList,
    this.freeListContent,
    this.vipList,
    this.vipListContent,
    this.lastViewId
  });

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);
  Map<String, dynamic> toJson() => _$BookToJson(this);
}
