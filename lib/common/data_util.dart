
import 'dart:convert';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/entity/book.dart';
import 'package:flutter_xinghai/model/json_and_model.dart';
import 'package:flutter_xinghai/sqflite/book_database.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


Future<JsonAndModel> getBook(String bookIndex, int bookSourceId) async{
  BookDataBase dataBase = BookDataBase();
  Future<Book> bookModel = dataBase.getBook(bookIndex, bookSourceId);
  //数据库查找
  if (bookModel is Book) {
    return JsonAndModel(status: 200, data: bookModel as Book);
  }
  //网络请求数据(数据库没有查找到)
  final response = await http.get('${Constant.hostUrl}/api/book/getBook?bookIndex=$bookIndex&bookSourceId=$bookSourceId');
  if (response.statusCode != 200) {
    return JsonAndModel(
      status: 404,msg: '当前网络不可用，请确认网络状态！',data: null,
    );
  }
  JsonAndModel jsonAndModel = JsonAndModel.fromJson(json.decode(response.body));
  //请求数据成功，保存到数据库
  if (jsonAndModel.status == 200) {
    dataBase.insertBook(json.decode(jsonAndModel.data));
  }
  return jsonAndModel;
}

/**
 * 从网络或缓冲中获取数据（当缓存数据过期就请求网络数据）
 * @param dataKey 缓存数据key
 * @param timeKey 缓存时间key
 * @param timeExpire 缓存时间
 * @param url 网络获取数据URL
 * @return JsonAndModel
 */
Future<JsonAndModel> myFetchData(String dataKey, String timeKey,int timeExpire, String getURL) async{
  //1.从缓存获取数据（过期或无数据则请求URL获取）
  SharedPreferences preferences = await SharedPreferences.getInstance();
  //1.1 获取当前时间戳
  int newTime = DateTime.now().millisecondsSinceEpoch;
  //1.2 获取缓存时间戳
  int oldTime = preferences.getInt(timeKey);
  //1.3 判断是否过期
  if (oldTime != null && (((newTime-oldTime)/timeExpire) < 1)) {
    //1.3.1 更新时间戳
    preferences.setInt(timeKey, newTime);
    //1.3.2 获取数据
    String data = preferences.getString(dataKey);
    if (data != null && data.isNotEmpty) {
      return JsonAndModel(status: Constant.statusCodeOK,msg:Constant.msgCodeOK,data: data);
    }
  }
  
  //2.请求数据
  //2.1 网络请求数据
  http.Response response;
  try {
    response = await http.get(getURL).timeout(Duration(seconds: 5));  //设置响应时间不超过5秒
  } catch (e) {
    return JsonAndModel(status: Constant.statusNoFound, msg: Constant.msgNoFound, data: null);
  }
  //2.2 数据校验
  //2.2.1 发送请求失败
  //2.2.2 请求数据成功，缓存数据
  JsonAndModel jsonAndModel = JsonAndModel.fromJson(json.decode(response.body));
  if (jsonAndModel.status == Constant.statusCodeOK) {
    preferences.setInt(timeKey, newTime);
    preferences.setString(dataKey, jsonAndModel.data.toString());
  }
  return jsonAndModel;
}
