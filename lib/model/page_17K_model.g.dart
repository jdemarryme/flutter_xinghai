// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'page_17K_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Page17KModel _$Page17KModelFromJson(Map<String, dynamic> json) {
  return Page17KModel(
      productHotNS: (json['productHotNS'] as List)
          ?.map((e) =>
              e == null ? null : BookModel.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      productHotVS: (json['productHotVS'] as List)
          ?.map((e) =>
              e == null ? null : BookModel.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      productHotGX: (json['productHotGX'] as List)
          ?.map((e) =>
              e == null ? null : BookModel.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      productNew: (json['productNew'] as List)
          ?.map((e) => (e as List)
              ?.map((e) => e == null
                  ? null
                  : BookModel.fromJson(e as Map<String, dynamic>))
              ?.toList())
          ?.toList(),
      productLike: (json['productLike'] as List)
          ?.map((e) =>
              e == null ? null : BookModel.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$Page17KModelToJson(Page17KModel instance) =>
    <String, dynamic>{
      'productHotNS': instance.productHotNS,
      'productHotVS': instance.productHotVS,
      'productHotGX': instance.productHotGX,
      'productNew': instance.productNew,
      'productLike': instance.productLike
    };
