import 'package:flutter/material.dart';
import 'dart:ui' as ui;

/**
 * 全屏透明高斯模糊图片背景
 */
class MyBlurImageContainer extends StatelessWidget {

  final String bookImageUrl;
  MyBlurImageContainer({this.bookImageUrl});

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(bookImageUrl),
            fit: BoxFit.fitHeight,
          ),
        ),
        child: BackdropFilter(
          filter: ui.ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            color: Colors.black26,
          ),
        ),
      ),
    );
  }
}