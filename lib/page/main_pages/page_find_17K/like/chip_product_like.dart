import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/common/widgets/my_image_bar.dart';
import 'package:flutter_xinghai/constant/resouce_constant.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/like/page_more_like.dart';

class ProductLikeChip extends StatelessWidget {
  final List<BookModel> bookList;
  ProductLikeChip({this.bookList});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        MyImageBar(
          titleAssetName: ResourceConstant.imageProductLike,
          onPressedMethod: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Page17KMoreLike(
              bookModelList: bookList,
              title: ResourceConstant.textProductLike,
          )))),
        Container(
          height: 170.0,
          color: Colors.transparent,
          child: PageView(
            controller: PageController(viewportFraction: 0.9),
            children: List.generate(bookList.length, (index) {
              BookModel bookModel = bookList[index];
              return Card(
                color: Colors.white.withOpacity(0.85),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    MyCachedBookImage(
                      bookIndex: bookModel.bookIndex,
                      bookImgUrl: bookModel.imageUrl,
                      bookSourceId: bookModel.sourceId,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          width: 120.0,
                          child: Text('${bookModel.title}', 
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Text('${bookModel.value}', style: TextStyle(fontSize: 17.0),),
                        Container(
                          width: 180.0,
                          child: Text('${bookModel.description}', 
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          )
                        ),
                      ],
                    ),
                  ],
                ),
              );
            }),
          ),
        ),
      ],
    );
  }
}
