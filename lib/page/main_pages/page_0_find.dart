import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/pages/my_no_developed_page.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/page_17K_main.dart';

/*
 * 底部导航栏[发现]界面 
 */
class PageFind extends StatefulWidget {
  _PageFindState createState() => _PageFindState();
}

class _PageFindState extends State<PageFind> with SingleTickerProviderStateMixin{

  /// 当前顶部导航栏的id
  int _index = 0;
  TabController _tabController;
  List<Widget> _tabViews;

  @override
  void initState() {
    super.initState();

    _tabViews = [
      Page17KMain(),
      PageNoDevelopedPage(),
      PageNoDevelopedPage(),
      PageNoDevelopedPage(),
    ];

    _tabController = TabController(
      vsync: this, initialIndex: _index, length: _tabViews.length,
    );
    _tabController.addListener(() {
      if (_index != _tabController.index) {
        setState(() {
         _index = _tabController.index; 
        });
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey, //修改顶部颜色
      body: ListView(
      children: <Widget>[
        Container(
              height: 64.0,
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Image.asset('assets/images/title.png', color: Colors.white,),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blueGrey,
                    Color.fromRGBO(58,95,131,1),
                  ]
                )
              ),
          ), 
        Container(
          height: 1410.0,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color.fromRGBO(58,95,131,1),
                Colors.blueGrey[200]
              ]
            )
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: TabBar(
              controller: _tabController,
              isScrollable: true, //可以滑动
              indicatorColor: Colors.white10,
              indicatorSize: TabBarIndicatorSize.label,
              tabs: <Widget>[
                Container(
                  child: _index == 0
                    ? Image.asset('assets/images/tabs2/actionTab0.png', height: 32.0,color: Colors.white,)
                    : Image.asset('assets/images/tabs2/tab0.png',color: Colors.white,),
                ), 
                Container(
                  child: _index == 1
                    ? Image.asset('assets/images/tabs2/actionTab1.png', height: 32.0,color: Colors.white,)
                    : Image.asset('assets/images/tabs2/tab1.png',color: Colors.white,),
                ),
                Container(
                  child: _index == 2
                    ? Image.asset('assets/images/tabs2/actionTab2.png', height: 32.0,color: Colors.white,)
                    : Image.asset('assets/images/tabs2/tab2.png',color: Colors.white,),
                ),
                Container(
                  child: _index == 3
                    ? Image.asset('assets/images/tabs2/actionTab3.png', height: 32.0,color: Colors.white,)
                    : Image.asset('assets/images/tabs2/tab3.png',color: Colors.white,),
                ),
                /* Container(
                  child: _index == 4
                    ? Image.asset('assets/images/tabs2/actionTab4.png', height: 32.0,)
                    : Image.asset('assets/images/tabs2/tab4.png'),
                ), */
              ],
            ),
            body: TabBarView(
              controller: _tabController,
              // physics: NeverScrollableScrollPhysics(),
              children: _tabViews,
            ),
          ),
        ),
      ],
    ),
    );
  }
}