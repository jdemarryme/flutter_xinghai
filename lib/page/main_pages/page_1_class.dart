import 'package:flutter/material.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/page/main_pages/page_class/page_class_item.dart';

class PageClass extends StatefulWidget {
  @override
  _PageClassState createState() => _PageClassState();
}

class _PageClassState extends State<PageClass> {

  List<Widget> itemList;

  @override
  void initState() {
    super.initState();
    itemList = new List();
  }

  @override
  Widget build(BuildContext context) {
    itemList = List.generate(9, (index) {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        child: Material(
          borderRadius: BorderRadius.circular(20.0),
          elevation: 14.0,
          shadowColor: Colors.grey.withOpacity(0.5),
          child: _myItem(context, index)
        ),
      );
    });
    itemList.add(
      Container(
        height: 50.0,
        child: Center(child: Text('٩(๑❛ᴗ❛๑)۶  到底儿了......', style: TextStyle(color: Colors.white),))
      )
    );
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: ListView(
          children: [
            Column(
              children: <Widget>[
                Container(
                  height: 54.0,
                  width: double.infinity,
                  child: Image.asset('assets/images/classify.png', color: Colors.white,),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.blueGrey,
                          Color.fromRGBO(58,95,131,1),
                        ]
                    )
                  ),
                ),
              ]
            ),
            Column(children: itemList,)
          ]
        ),
      );
  }
}

Widget _myItem(BuildContext context, int index) {
  return Stack(
    children: [
      Container(
        color: Colors.transparent,
        padding: EdgeInsets.all(8.0),
        height: 220.0,
        child: AspectRatio(
          aspectRatio: 16/10,
          child: Image.asset(
            'assets/images/tabs3/${index+1}.png',
            fit: BoxFit.cover,
          ),
        ),
      ),
      Positioned.fill(
        child: Material(
          color: Colors.transparent,
          shadowColor: Colors.white24,
          elevation: 14.0,
          child: InkWell(
            splashColor: Colors.white.withOpacity(0.5),
            highlightColor: Colors.white.withOpacity(0.1),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => PageClassItem(index:index)
                )
              );
            },
          ),
        ),
      ),
      Positioned(
        top: 5.0,
        left: 7.0,
        child:  Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(10.0)),
            color: Colors.white70,
          ),
          child: Text(' ${index+1} ${Constant.pageClassTitles[index]} ',
            style: TextStyle(
              fontWeight: FontWeight.bold, 
              color: Colors.blueGrey,
              fontSize: 22.0),
            ),
        ),
      )
    ]
  );
}