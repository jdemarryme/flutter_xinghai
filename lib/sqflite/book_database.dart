//Three Party Toolkit: sqflite database
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/entity/book.dart';
import 'package:sqflite/sqflite.dart';
//For the use of local file
import 'dart:io';
//Asynchronous operation
import 'dart:async';
//For the use of local path
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

class BookDataBase {
  Database _database;

  //Table Name of DataBase
  final String tableName = 'book';

  //Define fields
  //本地数据库唯一标识
  final String _id = "id"; 
  //远程数据库唯一标识
  final String _bookId = "bookId";
  //图书网站唯一标识
  final String _bookIndex = "bookIndex";
  //图书网站来源
  final String _bookSourceId = "bookSourceId";
  //图书封面链接
  final String _bookImgUrl = "bookImgUrl";
  //图书标题
  final String _bookName = "bookName";
  //图书简介
  final String _bookInfo = "bookInfo";
  //图书字数
  final String _bookWordCount = "bookWordCount";
  //图书最近一次更新时间
  final String _bookLastTime = "bookLastTime";
  //图书作者id
  final String _authorId = "authorId";
  //图书作者网站唯一标识
  final String _authorIndex = "authorIndex";
  //最近更新时间
  final String _pullLastTime = "pullLastTime";
  //免费章节
  final String _freeList = "freeList";
  //付费章节
  final String _vipList = "vipList";
  //免费章节内容
  final String _freeListContent = "freeListContent";
  //付费章节内容
  final String _vipListContent = "vipListContent";
  //最后一次观看
  final String _lastViewId = "lastViewId";


  //get database
  Future get db async {
    if (_database != null) {
      return _database;
    } else {
      _database = await initDB();
      return _database;
    }
  }

  //init database
  initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path =join(directory.path, '$tableName.db');
    var database = await openDatabase(path, version: 1, onCreate:_createDB);
    return database;
  }

  //create database
  FutureOr _createDB(Database database, int version) async {
    await database.execute('''
      create table $tableName(
        $_id integer primary key autoincrement,
        $_bookId integer not null,
        $_bookIndex text not null,
        $_bookSourceId integer not null,
        $_bookImgUrl text,
        $_bookName text,
        $_bookInfo text,
        $_bookWordCount integer,
        $_bookLastTime text,
        $_authorId integer,
        $_authorIndex text,
        $_pullLastTime text,
        $_freeList text,
        $_vipList text,
        $_freeListContent text,
        $_vipListContent text,
        $_lastViewId int)
    ''');
    http.get('${Constant.hostUrl}/api/source/increaseCount?check=asASDd123rSERf341ui');
    print('创建数据库');
  }

  //operation: insert
  Future<int> insertBook(Map bookModel) async {
    Database database = await db;
    int result = await database.insert(tableName, bookModel);
    print('插入数据，返回状态码：$result');
    return result;
  }

  //operation: select
  /*数据库查询操作返回的都是map类型的数组，
    map中的键就是查询对象，值就是查询的结果。
    哪怕查询结果只有一条返回的也是一个数组。*/
  Future<Book> getBook(String bookIndex, int bookSourceId) async {
    Database database = await db;
    List result = await database
      .rawQuery('''
        select * from $tableName 
        where $_bookIndex = $bookIndex
        and $_bookSourceId = $bookSourceId''');
    if (result.length == 0) {
      return null;
    } else {
      return Book.fromJson(result[0]);
    }
  }

  Future<Book> getBookById(String bookId) async {
    Database database = await db;
    List result = await database
      .rawQuery('''
        select * from $tableName
        where $_bookId = $bookId
      ''');
    if (result.length == 0) {
      return null;
    } else {
      return Book.fromJson(result[0]);
    }
  }

  Future<int> updateChapterById(String bookId, String chapter) async {
    Database database = await db;
    int result = await database
      .rawUpdate('update $tableName set $_freeListContent = ? where $_bookId = $bookId',[chapter]);
    print('修改数据，返回状态码：$result');
  }

  Future<List<Book>> getAllBook() async {
    Database database = await db;
    List result = await database
      .rawQuery('''
        select * from $tableName 
      ''');
    if (result.length == 0) {
      return null;
    } else {
      return List.generate(result.length, (index) {
        return Book.fromJson(result[index]);
      });
    }
  }
  //operation: delete
  Future<int> deletAll() async {
    Database database = await db;
    int result = await database
      .rawDelete('''
        delete * from $tableName
      ''');
    return result;
  }

  //关闭数据库
  Future close() async {
    Database database = await db;
    database.close();
  }

}

