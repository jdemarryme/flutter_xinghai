import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

@JsonSerializable()
class User {
  String userId;
  String mail;
  String pwd;
  String nickname;
  String sex;
  String birth;
  String viewHistoryList;
  String registerTime;

  User({
    this.userId,
    this.mail,
    this.pwd,
    this.nickname,
    this.sex,
    this.birth,
    this.viewHistoryList,
    this.registerTime
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
