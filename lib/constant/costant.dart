
class Constant {
  // static final String hostUrl = "http://10.0.2.2:8090/xinghai";
  static final String hostUrl = "http://47.106.155.98:8090/xinghai";

  static final String page0FindKey = "page_0_find_key";
  static final String page0TimeKey = "page_0_time_key";
  
  /// 底部导航菜单-分类-详细分类，URL请求参数为index（分类标号）
  static final String page1ClassURL = '$hostUrl/api/util/getClassifyList';
  static final String page1ClassifyKey = "page_1_classify_key";
  static final String page1TimeKey = "page_1_time_key";
  
  
  static final String page2RecommendKey = "page_2_recommend_key";
  static final String page2TimeKey = "page_2_time_key";
  static final String userKey = "user_key";

  /// 获取图书的详细信息，URL请求参数为bookIndex（图书唯一标识）,index（图书来源）
  static final String getBookURL = '$hostUrl/api/book/getBook';

  /// 获取章节的详细内容，URL请求参数为id（默认值填0）、bookId（图书id）、indexId（章节顺序id）
  static final String getChapterURL = '$hostUrl/api/book/getChapter';

  
  /// 过期时间为3天
  static final int timeExpire3Days = 259200000;
  /// 过期时间为1天
  static final int timeExpire1Day = 86400000;

  

  static const int netWorkUnConnected = -2;
  static const int netWorkUnSuccessful = -1;

  static const int statusCodeOK = 200;
  static const String msgCodeOK = '请求成功';
  static const int statusNoFound = 404;
  static const String msgNoFound = '请确认当前网络状态已连接';

  static const List<String> pageClassTitles = [
    '玄幻奇幻','历史穿越','古装言情','唯美纯爱','仙侠武侠','虚拟世界','都市情怀','创世末世','恐怖悬疑'
  ];

  static const List<String> sourceList = [
    '17K小说网','起点中文网','纵横中文','小说阅读'
  ];

  static const String myFontSizeKey = 'myFontSizeKey';
  static const String myColorListIndexKey = 'myColorListIndexKey';
}