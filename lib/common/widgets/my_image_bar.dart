import 'package:flutter/material.dart';
import 'package:flutter_xinghai/constant/resouce_constant.dart';
class MyImageBar extends StatelessWidget {
  /// 标题本地链接名
  final String titleAssetName;
  /// 点击更多触发的事件
  final VoidCallback onPressedMethod;
  MyImageBar({this.titleAssetName, this.onPressedMethod});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            child: Image.asset(titleAssetName, color: Colors.white,),
            padding: EdgeInsets.only(left: 16.0),
          ),
          FlatButton(
            child: Image.asset(ResourceConstant.imageMore, color: Colors.white70,),
            onPressed: onPressedMethod,
          ),
        ],
      ),
    );
  }
}