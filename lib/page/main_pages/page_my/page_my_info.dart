import 'package:flutter/material.dart';

class PageMyInfo extends StatefulWidget {
  @override
  _PageMyInfoState createState() => _PageMyInfoState();
}

class _PageMyInfoState extends State<PageMyInfo> {
  @override
  Widget build(BuildContext context) {
  return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(58,95,131,1),
        title: Text('关于我们',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      backgroundColor: Color.fromRGBO(58,95,131,1),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              '''    星海阅读软件使用最新的Flutter框架，可实现安卓和苹果应用的跨平台。星海阅读是我们的课程设计，仅仅提供学习参考，不提供商业服务。    
      我们是来自西南科技大学计算机科学与技术学院计科卓越1601的学生，热爱编程。如有疑问请联系我们，我们的联系方式为：app_xinghai@163.com
              ''', style: TextStyle(fontSize: 22.0, color: Colors.white),
            ),
            Text('2019年3月15日',
              style: TextStyle(fontSize: 20.0, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}