import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/pages/my_error_page.dart';
import 'package:flutter_xinghai/common/pages/my_waiting_page.dart';
import 'package:flutter_xinghai/common/widgets/my_blur_image_container.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/entity/book.dart';
import 'package:flutter_xinghai/model/json_and_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_book/chapter_book_item.dart';
import 'package:flutter_xinghai/sqflite/book_database.dart';
import 'package:http/http.dart' as http;

class PageBookItem extends StatefulWidget {
  final String bookIndex;
  final int bookSourceId;
  PageBookItem({this.bookIndex, this.bookSourceId});
  @override
  _PageBookItemState createState() =>
      _PageBookItemState(bookIndex: bookIndex, bookSourceId: bookSourceId);
}

class _PageBookItemState extends State<PageBookItem> {
  String bookIndex;
  int bookSourceId;
  _PageBookItemState({this.bookIndex, this.bookSourceId});

  Future<JsonAndModel> _fetchBookData() async {
    final BookDataBase bookDataBase = BookDataBase();
    // final AuthorDataBase authorDataBase = AuthorDataBase();

    /*  List<Book> bookList = await bookDataBase.getAllBook();
    debugPrint(bookList.toList().toString());  //输出所有图书 */

    Book book = await bookDataBase.getBook(bookIndex, bookSourceId);
    if (book != null) {
      // debugPrint('数据库找到该书');
      return JsonAndModel(
          status: Constant.statusCodeOK, msg: Constant.msgCodeOK, data: book);
    }
    // debugPrint('数据库未找到该书');
    http.Response response;
    try {
      String url =
          '${Constant.getBookURL}?bookIndex=$bookIndex&bookSourceId=$bookSourceId';
      response =
          await http.get(url).timeout(Duration(seconds: 7)); //设置响应时间不超过5秒
    } catch (e) {
      return JsonAndModel(
          status: Constant.statusNoFound, msg: Constant.msgNoFound, data: null);
    }
    JsonAndModel jsonAndModel =
        JsonAndModel.fromJson(json.decode(response.body));
    // debugPrint(jsonAndModel.toJson().toString());
    if (jsonAndModel.status == Constant.statusCodeOK) {
      Map bookMap = jsonAndModel.data['wbook']; //缺损author值
      bookDataBase.insertBook(bookMap);
      return JsonAndModel(
          status: Constant.statusCodeOK,
          msg: Constant.msgCodeOK,
          data: Book.fromJson(bookMap));
    }
    return jsonAndModel;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fetchBookData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MyWaitingPage();
        }
        JsonAndModel jsonAndModel = snapshot.data;
        switch (jsonAndModel.status) {
          case Constant.statusCodeOK:
            return MyBookPage(book: jsonAndModel.data);
          case Constant.statusNoFound:
            return MyErrorPage(
              title: '获取数据失败',
              info: '${jsonAndModel.msg}',
              method: () {
                setState(() {});
              },
              popContext: true,
            );
          default:
            return MyErrorPage(
              title: '服务器出错',
              info: '错误原因：${jsonAndModel.msg}',
              method: () {
                setState(() {});
              },
              popContext: true,
            );
        }
      },
    );
  }
}

class MyBookPage extends StatelessWidget {
  final Book book;
  MyBookPage({this.book});
  bool isFree = false;
  double vipLength;

  @override
  Widget build(BuildContext context) {
    final String freeString = book.freeList;
    final String vipList = book.vipList;  //付费章节书目
    List<String> freeList = freeString.split("\$"); //免费章节列表
    if (vipList.isEmpty || vipList == "0") {
      isFree = true;
    } else {
      vipLength = double.parse(vipList) / (freeList.length + double.parse(vipList));
    }
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            //模糊背景
            MyBlurImageContainer(bookImageUrl: book.bookImgUrl),
            //左上角
            Positioned(
              top: 25.0,
              left: 0.0,
              child: Container(
                color: Colors.white54,
                child: Row(
                  children: <Widget>[
                    FlatButton.icon(
                      icon: Icon(Icons.arrow_back_ios),
                      label: Text('${book.bookName}',
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 20.0
                        ),
                      ),
                      onPressed:  () => Navigator.of(context).pop(),
                    ),
                  ],
                ),
              ),
            ),
            //主题
            Column(
              children: <Widget>[
                SizedBox(height: 85.0,),
                Container(
                  padding: EdgeInsets.all(10.0),
                  color: Colors.white70,
                  width: double.infinity,
                  child: Row(
                    children: <Widget>[
                      MyCachedBookImage(
                        bookImgUrl: book.bookImgUrl,
                        bookIndex: book.bookIndex,
                        bookSourceId: '${book.bookSourceId}',
                        height: 154, width: 110
                      ),
                      SizedBox(width: 10.0,),
                      Expanded(
                        child: Text('图书简介：${book.bookInfo}',
                          maxLines: 7,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    // height: 300.0,
                    color: Colors.white54,
                    child: DefaultTabController(
                      length: 2,
                      child: Scaffold(
                        backgroundColor: Colors.transparent,
                        appBar: TabBar(
                          indicatorColor: Colors.black87,
                          indicatorSize: TabBarIndicatorSize.label,
                          tabs: <Widget>[
                            Text(
                              '免费目录',
                              style: TextStyle(fontSize: 17.0),
                            ),
                            Text(
                              '付费目录',
                              style: TextStyle(fontSize: 17.0),
                            )
                          ],
                        ),
                        body: TabBarView(
                          children: <Widget>[
                            SingleChildScrollView(
                              child: Wrap(
                                spacing: 4.0,
                                children: List.generate(freeList.length - 1, (index) {
                                  return ActionChip(
                                    label: Text(freeList[index]),
                                    onPressed: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => 
                                            ChapterBookItem(id: "0", indexId: '$index',bookId: '${book.bookId}')
                                        )
                                      );
                                    },
                                  );
                              })),
                            ),
                            isFree
                            ? Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Icon(Icons.local_florist, size: 128, color: Colors.black26,),
                                  Text('该书是免费图书，没有付费章节哦', 
                                    style: TextStyle(
                                      fontSize: 20.0
                                    ),
                                  )
                                ],
                              )
                            : Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Icon(Icons.book, size: 128, color: Colors.black26,),
                                Text('该图书含有付费章节$vipList 章',
                                  style: TextStyle(
                                    fontSize: 20.0
                                  ),
                                ),
                                Text('付费占总体的${(vipLength * 100).toStringAsFixed(1)}%',
                                  style: TextStyle(
                                    fontSize: 20.0
                                  ),
                                ),
                                Text('本软件只提供免费章节预览，付费章节请前往相关网站阅读！')
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 50.0,),
              ],
            ),
            Positioned(
              bottom: 0.0,
              right: 0.0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                decoration: BoxDecoration(
                  color: Colors.white54,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16.0)
                  )
                ),
                child: Text('本小说来自“${Constant.sourceList[book.bookSourceId-1]}”'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

