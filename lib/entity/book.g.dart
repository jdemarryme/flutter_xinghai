// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Book _$BookFromJson(Map<String, dynamic> json) {
  return Book(
      id: json['id'] as int,
      bookId: json['bookId'] as int,
      bookIndex: json['bookIndex'] as String,
      bookSourceId: json['bookSourceId'] as int,
      bookImgUrl: json['bookImgUrl'] as String,
      bookName: json['bookName'] as String,
      bookInfo: json['bookInfo'] as String,
      bookWordCount: json['bookWordCount'] as int,
      bookLastTime: json['bookLastTime'] as String,
      authorId: json['authorId'] as int,
      authorIndex: json['authorIndex'] as String,
      pullLastTime: json['pullLastTime'] as String,
      freeList: json['freeList'] as String,
      freeListContent: json['freeListContent'] as String,
      vipList: json['vipList'] as String,
      vipListContent: json['vipListContent'] as String,
      lastViewId: json['lastViewId'] as int);
}

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'id': instance.id,
      'bookId': instance.bookId,
      'bookIndex': instance.bookIndex,
      'bookSourceId': instance.bookSourceId,
      'bookImgUrl': instance.bookImgUrl,
      'bookName': instance.bookName,
      'bookInfo': instance.bookInfo,
      'bookWordCount': instance.bookWordCount,
      'bookLastTime': instance.bookLastTime,
      'authorId': instance.authorId,
      'authorIndex': instance.authorIndex,
      'pullLastTime': instance.pullLastTime,
      'freeList': instance.freeList,
      'vipList': instance.vipList,
      'freeListContent': instance.freeListContent,
      'vipListContent': instance.vipListContent,
      'lastViewId': instance.lastViewId
    };
