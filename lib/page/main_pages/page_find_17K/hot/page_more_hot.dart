import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_book/page_book_item.dart';

class Page17KMoreHot extends StatefulWidget {
  Page17KMoreHot({
    @required this.title,
    @required this.bookModelList,
  });
  final String title;
  final List<List<BookModel>> bookModelList;

  @override
  _Page17KMoreHotState createState() => _Page17KMoreHotState(
    title: title,
    bookModelList: bookModelList
  );
}

class _Page17KMoreHotState extends State<Page17KMoreHot> with SingleTickerProviderStateMixin{
  TabController _tabController;
  final String title;
  final List<List<BookModel>> bookModelList;
  int _index = 0;

  _Page17KMoreHotState({
    @required this.title,
    @required this.bookModelList,
  });

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this, initialIndex: _index, length: bookModelList.length
    );
    _tabController.addListener((){
      if (_index !=_tabController.index) {
        setState(() {
          _index = _tabController.index;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: 820.0,
          width: 450.0,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 0.0,
                child: Container(
                  height: 820.0,
                  width: 450.0,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blueGrey[300],
                        Color.fromRGBO(58,95,131,1),
                      ]
                    ),
                  ),
                )
              ),
              Container(
                height: 820.0,
                width: 450.0, //宽度修改
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 70.0,),
                    Container(
                      height: 50.0,
                      child: TabBar(
                        controller: _tabController,
                        indicatorColor: Colors.black,
                        indicatorSize: TabBarIndicatorSize.label,
                        labelStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                        unselectedLabelStyle: TextStyle(color: Colors.black),
                        tabs: <Widget>[
                          Text('男生点击榜', style: TextStyle(color: Colors.black,fontSize: 16.0),),
                          Text('女生点击榜', style: TextStyle(color: Colors.black,fontSize: 16.0),),
                          Text('个性点击榜', style: TextStyle(color: Colors.black,fontSize: 16.0),),
                        ],
                      ),
                    ),
                    Container(
                      height: 700.0,
                      width: 450.0,
                      child: TabBarView(
                        controller: _tabController,
                        children: _getPageList(context,bookModelList),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 30.0,
                left: 0.0,
                child: IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.white,),
                  onPressed:  () => Navigator.of(context).pop(),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 40.0),
                  child: Text(title,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

List<Widget> _getPageList(BuildContext context, List<List<BookModel>> bookModelList) {
  return List.generate(bookModelList.length, (index) {
    return Column(
      children: _getListTlieList(context, bookModelList[index]),
    );
  });
}

List<Widget> _getListTlieList(BuildContext context, List<BookModel> bookModelList) {
  return List.generate(bookModelList.length, (index) {
    BookModel bookModel = bookModelList[index];
    if (index == 0) {
      return Container(
        height: 240.0,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0,),
            _getListItem(context, bookModelList[index], index),
            Container(
              height: 180.0,
              color: Colors.white24,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  MyCachedBookImage(
                    bookIndex: bookModel.bookIndex,
                    bookSourceId: bookModel.sourceId,
                    bookImgUrl: bookModel.imageUrl, 
                  ),
                  Container(
                    width: 220.0,
                    height: 120.0,
                    child: Text('    ${bookModelList[index].description}', style: TextStyle(fontSize: 16.0),),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
    return _getListItem(context, bookModelList[index], index);
  });
}

Widget _getListItem(BuildContext context, BookModel bookItem, int index) {
  return FlatButton(
    onPressed: () => 
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => PageBookItem(bookIndex: bookItem.bookIndex, bookSourceId: int.parse(bookItem.sourceId),)
        )
      ),
      child: Container(
      height: 45.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(child: Center(child: Text('${index+1}')), width: 50.0,),
          Container(child: Row(
            children: <Widget>[
              Text('${bookItem.type}  ',),
              Container(
                child: Text('${_getAreaTitle(bookItem.title)}', 
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, ),
                  overflow: TextOverflow.ellipsis,),
              ),
            ],
          ), width: 200.0,), 
          Container(child: Center(child: Text(bookItem.value)), width: 60.0,),
        ],
      ),
    ),
  );
}

String _getAreaTitle(String title) {
  if (title.length > 7) {
    return '${title.substring(0,7)}...';
  } else {
    return title;
  }
}