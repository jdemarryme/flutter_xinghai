import 'package:flutter_xinghai/model/book_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'page_17K_model.g.dart';

@JsonSerializable()

class Page17KModel {
  //热门作品之男生
  List<BookModel> productHotNS;
  //热门作品之女生
  List<BookModel> productHotVS;
  //热门作品之个性
  List<BookModel> productHotGX;
  //最新作品
  List<List<BookModel>> productNew;
  //推荐作品
  List<BookModel> productLike;

  Page17KModel({
    this.productHotNS,
    this.productHotVS,
    this.productHotGX,
    this.productNew,
    this.productLike
  });

  factory Page17KModel.fromJson(Map<String, dynamic> json) => _$Page17KModelFromJson(json);
  Map<String, dynamic> toJson() => _$Page17KModelToJson(this);
}
