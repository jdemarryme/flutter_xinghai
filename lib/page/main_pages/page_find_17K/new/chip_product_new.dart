import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/common/widgets/my_image_bar.dart';
import 'package:flutter_xinghai/constant/resouce_constant.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/new/page_more_new.dart';

class ProductNewChip extends StatelessWidget {
  final List<List<BookModel>> bookList;
  ProductNewChip({this.bookList});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        MyImageBar(
          titleAssetName: ResourceConstant.imageProductNew,
          onPressedMethod: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Page17KMoreNew(
              bookModelList: bookList,
              title: ResourceConstant.textProductNew,
          )))),
        Container(
          height: 360.0,
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: getBookImageWithTextList(bookList[1]),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: getBookImageWithTextList(bookList[2]),
              ),
            ],
          )),
      ],
    );
  }
}

List<Widget> getBookImageWithTextList(List<BookModel> bookModelList) {
  return List.generate(bookModelList.length - 1, (int index) {
    BookModel bookModel = bookModelList[index+1];
    return Container(
      height: 180.0,
      width: 100.0,
      child: Column(
        children: <Widget>[
          MyCachedBookImage(
            bookImgUrl: bookModel.imageUrl,
            bookIndex: bookModel.bookIndex,
            bookSourceId: bookModel.sourceId,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            bookModelList[index + 1].title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  });
}