// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_and_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonAndModel _$JsonAndModelFromJson(Map<String, dynamic> json) {
  return JsonAndModel(
      status: json['status'] as int,
      msg: json['msg'] as String,
      data: json['data']);
}

Map<String, dynamic> _$JsonAndModelToJson(JsonAndModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
      'data': instance.data
    };
