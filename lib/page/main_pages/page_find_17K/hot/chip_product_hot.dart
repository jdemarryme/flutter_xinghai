import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/common/widgets/my_image_bar.dart';
import 'package:flutter_xinghai/constant/resouce_constant.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'package:flutter_xinghai/model/page_17K_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/hot/page_more_hot.dart';

class ProductHotChip extends StatelessWidget {
  final Page17KModel page17kModel;
  ProductHotChip({this.page17kModel});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        MyImageBar(
          titleAssetName: ResourceConstant.imageProductHot,
          onPressedMethod: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => Page17KMoreHot(
                title: ResourceConstant.textProductHot,
                bookModelList: [
                  page17kModel.productHotNS, 
                  page17kModel.productHotVS, 
                  page17kModel.productHotGX],
              ))
            ),),
        Container(
            height: 520.0,
            color: Colors.transparent,
            child: Column(
              children: getBookColumnList([
                page17kModel.productHotNS[0],
                page17kModel.productHotVS[0],
                page17kModel.productHotGX[0]
              ]),
            ),
          ),

      ],
    );
  }
}

List<Widget> getBookColumnList(List<BookModel> bookModelList) {
  return List.generate(bookModelList.length, (int index) {
    BookModel bookModel = bookModelList[index];
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 8.0, right: 16.0, top: 8.0),
            child: MyCachedBookImage(
              bookImgUrl: bookModel.imageUrl,
              bookIndex: bookModel.bookIndex,
              bookSourceId: bookModel.sourceId,
            ),
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Text(bookModelList[index].title,
                        overflow: TextOverflow.ellipsis,  //解决字体超长造成的越界
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19.0)),
                    ),
                    Text(
                      '网站点击量：${bookModelList[index].value}',
                      style: TextStyle(color: Colors.black45, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                SizedBox(height: 4.0,),
                Text(
                  bookModelList[index].description,maxLines: 3,
                  overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 15.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  });
}