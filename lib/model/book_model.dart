import 'package:json_annotation/json_annotation.dart';
part 'book_model.g.dart';

@JsonSerializable()
class BookModel {
  String imageUrl;
  String description;
  String title;
  String value;
  String bookIndex;
  String sourceId;
  String type;

  BookModel({
    this.imageUrl,
    this.description,
    this.title,
    this.value,
    this.bookIndex,
    this.sourceId,
    this.type
  });

  factory BookModel.fromJson(Map<String, dynamic> json) => _$BookModelFromJson(json);
  Map<String, dynamic> toJson() => _$BookModelToJson(this);
}
