import 'package:flutter/material.dart';

class MyErrorPage extends StatelessWidget {

  /// 提示标题
  final String title;
  /// 提示信息
  final String info;
  /// 回调方法
  final VoidCallback method;
  /// 是否启用返回
  final bool popContext;
  

  MyErrorPage({this.title, this.info, this.method, this.popContext = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.blueGrey,
              Color.fromRGBO(58,95,131,1),
            ]
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 70.0,),
            Container(
              height: 240.0,
              width: double.infinity,
              margin: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                color: Colors.white12,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: 64.0,
                    width: double.infinity,
                    child: Center(
                      child: Icon(
                        Icons.error_outline, 
                        color: Colors.white54,
                        size: 54.0,
                      ),
                    )
                  ),
                  Text(title, 
                    style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  Text(info,
                    style: TextStyle(
                      color: Colors.white70,
                      fontSize: 20.0
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20.0,),
            popContext
            ?
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                FlatButton(
                  color: Colors.blueGrey[400],
                  child: Text('点击刷新',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),
                  ),
                  onPressed: method,
                ),
                FlatButton(
                  color: Colors.blueGrey[400],
                  child: Text('返回上一页',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),
                  ),
                  onPressed: () => Navigator.pop(context),
                )
              ],
            )
            :
            Center(
              child: FlatButton(
                  color: Colors.blueGrey[400],
                  child: Text('点击刷新',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),
                  ),
                  onPressed: method,
                ),
            )
          ],
        ),
      ),
    );
  }
}