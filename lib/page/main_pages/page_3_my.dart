import 'package:flutter/material.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/page/login_page.dart';
import 'package:flutter_xinghai/page/main_pages/page_my/page_my_attention.dart';
import 'package:flutter_xinghai/page/main_pages/page_my/page_my_info.dart';
import 'package:flutter_xinghai/page/main_pages/page_my/page_my_item.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageMy extends StatefulWidget {
  @override
  _PageMyState createState() => _PageMyState();
}

class _PageMyState extends State<PageMy> {

  bool test = false;

  @override
  void initState() {
    super.initState();
    fetchUser();
  }

  fetchUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String data = preferences.getString(Constant.userKey);
    if (data != null && data.isNotEmpty) {
      test = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        title: Text('个人中心', 
          style: TextStyle(color: Colors.white,
            fontWeight: FontWeight.bold
          ),
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(58,95,131,1),
        actions: <Widget>[
          FlatButton(
            child: Text('登录',
              style: TextStyle(color: Colors.white,
                fontSize: 18.0
              ),
            ),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => LoginPage())
            ),
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(height: 20.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(width: 20.0,),
              Container(
                height: 200.0,
                width: 300.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16.0),
                  color: Color.fromRGBO(58,95,131,1)
                ),
                child: Center(
                  child: test ?
                  Image.asset('assets/images/heads/head_boy_0.jpg')
                  : Icon(Icons.account_circle,
                    color: Colors.white70,
                    size: 128.0,
                  ),
                ),
              ),
              SizedBox(width: 20.0,),
            ],
          ),
          SizedBox(height: 20.0,),
          GestureDetector(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PageMyItem())
            ),
            child: Container(
              height: 40.0,
              decoration: BoxDecoration(
                color: Color.fromRGBO(58,95,131,1),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(Icons.history, color: Colors.white,),
                  Text('浏览记录',
                    style: TextStyle(color: Colors.white,
                      fontSize: 18.0
                    ),
                  ),
                  SizedBox(width: 220.0,),
                  Icon(Icons.arrow_forward_ios,color: Colors.white),
                ],
              ),
            ),
          ),
          SizedBox(height: 20.0,),
          GestureDetector(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PageMyInfo())
            ),
            child: Container(
              height: 40.0,
              decoration: BoxDecoration(
                color: Color.fromRGBO(58,95,131,1),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(Icons.info_outline, color: Colors.white,),
                  Text('关于我们',
                    style: TextStyle(color: Colors.white,
                      fontSize: 18.0
                    ),
                  ),
                  SizedBox(width: 220.0,),
                  Icon(Icons.arrow_forward_ios,color: Colors.white),
                ],
              ),
            ),
          ),
           SizedBox(height: 20.0,),
          GestureDetector(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PageMyAttention())),
            child: Container(
              height: 40.0,
              decoration: BoxDecoration(
                color: Color.fromRGBO(58,95,131,1),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(Icons.perm_device_information, color: Colors.white,),
                  Text('免责声明',
                    style: TextStyle(color: Colors.white,
                      fontSize: 18.0
                    ),
                  ),
                  SizedBox(width: 220.0,),
                  Icon(Icons.arrow_forward_ios,color: Colors.white),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}