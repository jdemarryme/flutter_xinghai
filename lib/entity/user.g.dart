// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
      userId: json['userId'] as String,
      mail: json['mail'] as String,
      pwd: json['pwd'] as String,
      nickname: json['nickname'] as String,
      sex: json['sex'] as String,
      birth: json['birth'] as String,
      viewHistoryList: json['viewHistoryList'] as String,
      registerTime: json['registerTime'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'userId': instance.userId,
      'mail': instance.mail,
      'pwd': instance.pwd,
      'nickname': instance.nickname,
      'sex': instance.sex,
      'birth': instance.birth,
      'viewHistoryList': instance.viewHistoryList,
      'registerTime': instance.registerTime
    };
