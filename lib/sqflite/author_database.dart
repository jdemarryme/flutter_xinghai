import 'package:flutter/material.dart';
import 'package:flutter_xinghai/entity/author.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'dart:async';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class AuthorDataBase {
  Database _database;

  /// 表名
  final String tableName = 'author';

  /// 本地数据库唯一标识
  final String _id = "id"; 
  /// 远程数据库唯一标识
  final String _authorId = "authorId";
  /// 图书网站作者唯一标识
  final String _authorIndex = "authorIndex";
  /// 图书网站作者来源
  final String _authorSourceId = "authorSourceId";
  /// 图书封面链接
  final String _authorImgUrl = "authorImgUrl";
  /// 作者昵称
  final String _authorName = "authorName";
  /// 作者作品列表
  final String _authorBookIndexList = "authorBookIndexList";


  //get database
  Future get db async {
    if (_database != null) {
      return _database;
    } else {
      _database = await initDB();
      return _database;
    }
  }

  //init database
  initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path =join(directory.path, '$tableName.db');
    var database = await openDatabase(path, version: 1, onCreate:_createDB);
    return database;
  }

  //create database
  FutureOr _createDB(Database database, int version) async {
    await database.execute('''
      create table $tableName(
        $_id integer primary key autoincrement,
        $_authorId integer,
        $_authorIndex text,
        $_authorSourceId integer,
        $_authorImgUrl text,
        $_authorName text,
        $_authorBookIndexList text)
    ''');
  }

  //operation: insert
  Future<int> insertAuthor(Map authorMap) async {
    Database database = await db;
    int result = await database.insert(tableName, authorMap);
    return result;
  }

  /**
   * 插入author数据，如果发现已经存在则不插入
   */
  Future<int> insert(Map map) async {
    if (getAuthor(map['authorIndex'], map['authorSourceId']) == null) {
      debugPrint('插入读者数据成功！');
      return insertAuthor(map);
    } else {
      return 0;
    }
  }

  //operation: select
  Future<Author> getAuthor(String authorIndex, int authorSourceId) async {
    Database database = await db;
    List result = await database
      .rawQuery('''
        select * from $tableName 
        where $_authorIndex = $authorIndex
        and $_authorSourceId = $authorSourceId''');
    if (result.length == 0) {
      return null;
    } else {
      return Author.fromJson(result[0]);
    }
  }

  Future<List<Author>> getAllAuthor() async {
    Database database = await db;
    List result = await database
      .rawQuery('''
        select * from $tableName 
      ''');
    if (result.length == 0) {
      return null;
    } else {
      return List.generate(result.length, (index) {
        return Author.fromJson(result[index]);
      });
    }
  }

  //关闭数据库
  Future close() async {
    Database database = await db;
    database.close();
  }

}

