// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Author _$AuthorFromJson(Map<String, dynamic> json) {
  return Author(
      id: json['id'] as int,
      authorSourceId: json['authorSourceId'] as int,
      authorImgUrl: json['authorImgUrl'] as String,
      authorId: json['authorId'] as int,
      authorIndex: json['authorIndex'] as String,
      authorBookIndexList: json['authorBookIndexList'] as String,
      authorName: json['authorName'] as String);
}

Map<String, dynamic> _$AuthorToJson(Author instance) => <String, dynamic>{
      'id': instance.id,
      'authorId': instance.authorId,
      'authorIndex': instance.authorIndex,
      'authorSourceId': instance.authorSourceId,
      'authorImgUrl': instance.authorImgUrl,
      'authorName': instance.authorName,
      'authorBookIndexList': instance.authorBookIndexList
    };
