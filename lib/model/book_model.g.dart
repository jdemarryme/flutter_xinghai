// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookModel _$BookModelFromJson(Map<String, dynamic> json) {
  return BookModel(
      imageUrl: json['imageUrl'] as String,
      description: json['description'] as String,
      title: json['title'] as String,
      value: json['value'] as String,
      bookIndex: json['bookIndex'] as String,
      sourceId: json['sourceId'] as String,
      type: json['type'] as String);
}

Map<String, dynamic> _$BookModelToJson(BookModel instance) => <String, dynamic>{
      'imageUrl': instance.imageUrl,
      'description': instance.description,
      'title': instance.title,
      'value': instance.value,
      'bookIndex': instance.bookIndex,
      'sourceId': instance.sourceId,
      'type': instance.type
    };
