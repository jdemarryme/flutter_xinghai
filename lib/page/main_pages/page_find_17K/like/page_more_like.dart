import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/model/book_model.dart';
import 'dart:ui' as ui;

class Page17KMoreLike extends StatelessWidget {

  Page17KMoreLike({
    @required this.bookModelList,
    @required this.title,
  });

  final List<BookModel> bookModelList;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: PageView(
          children: List.generate(bookModelList.length, (index) {
            BookModel bookModel = bookModelList[index];
            return Stack(
            children: <Widget>[
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(bookModel.imageUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: BackdropFilter(
                    filter: ui.ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Container(
                      color: Colors.white10,
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                    MyCachedBookImage(
                    bookIndex: bookModel.bookIndex,
                    bookSourceId: bookModel.sourceId,
                    bookImgUrl: bookModel.imageUrl, 
                    height: 210.0, width: 150.0
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white70,
                    ),
                    child: Column(
                      children: <Widget>[
                        Text(
                          bookModel.type, 
                          style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0
                          )
                        ),
                        Text(bookModel.value,
                          style: TextStyle(
                            fontSize: 16.0, color: Colors.black54
                          ),
                        ),
                        Text('    ${bookModel.description}', 
                          maxLines: 6, 
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 16.0),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.all(10.0),
                  ),
                ],
              ),
              Positioned(
                top: 30.0,
                left: 0.0,
                child: Container(
                  color: Colors.white54,
                  child: IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    onPressed:  () => Navigator.of(context).pop(),
                  ),
                ),
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Container(
                  color: Colors.white54,
                  child: Text('${index+1} / ${bookModelList.length}',
                  ),
                ),
              ),
              ],
            );
          }).toList()
        ),
      )
    );
  }
}

/* 
 * 已弃用，弃用原因：使用SizedBox()进行堆砌，不能很好地适配
List<Widget> _getBookCardList(List<BookModel> bookModelList) {
  return List.generate(bookModelList.length, (index) {
    BookModel bookModel = bookModelList[index];
    return Container(
      height: 680.0,
      width: 400.0,
      padding: EdgeInsets.only(top: 100.0),
      child: Column(
        children: <Widget>[
          MyCachedBookImage(
            bookIndex: bookModel.bookIndex,
            bookSourceId: bookModel.sourceId,
            bookImgUrl: bookModel.imageUrl, 
            height: 210.0, width: 150.0
          ),
          SizedBox(height: 18.0,),
          Text(bookModelList[index].title, style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.white),),
          SizedBox(height: 4.0,),
          Text(bookModelList[index].value, style: TextStyle(color: Colors.white,fontSize: 15.0, fontWeight: FontWeight.bold),),
          SizedBox(height: 30.0,),
          Text(bookModelList[index].type, style:TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
          SizedBox(height: 10.0,),
          Container(
            child: Text('    ${bookModelList[index].description}', maxLines: 8, style: TextStyle(fontSize: 16.0,),),
            padding: EdgeInsets.all(10.0),),
          Text('${index+1}/${bookModelList.length}'),
        ],
      ),
    );
  });
} */