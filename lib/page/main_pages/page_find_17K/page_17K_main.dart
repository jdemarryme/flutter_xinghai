import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_xinghai/common/data_util.dart';
import 'package:flutter_xinghai/common/pages/my_error_page.dart';
import 'package:flutter_xinghai/common/pages/my_waiting_page.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/model/json_and_model.dart';
import 'package:flutter_xinghai/model/page_17K_model.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/hot/chip_product_hot.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/like/chip_product_like.dart';
import 'package:flutter_xinghai/page/main_pages/page_find_17K/new/chip_product_new.dart';
import 'dart:convert';

class Page17KMain extends StatefulWidget {
  @override
  _Page17KMainState createState() => _Page17KMainState();
}

class _Page17KMainState extends State<Page17KMain> {

  Page17KModel page17kModel;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: myFetchData(
        Constant.page0FindKey, //缓存数据的key
        Constant.page0TimeKey,  //缓存数据的时间key
        Constant.timeExpire1Day,  //缓存数据的缓存时间
        '${Constant.hostUrl}/api/util/get17KPageList'  //请求网络数据地址
      ),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MyWaitingPage();
        }
        JsonAndModel jsonAndModel = snapshot.data;
        switch (jsonAndModel.status) {
          case Constant.statusCodeOK:
            return _myFindPage(jsonAndModel.data.toString());
          case Constant.statusNoFound:
            return MyErrorPage(title: '获取数据失败',info: '${jsonAndModel.msg}',method: () {setState(() {});},);
          default:
            return MyErrorPage(title: '服务器出错',info: '错误原因：${jsonAndModel.msg}', method: () {setState(() {});},);
        }
      },
    );
  }

  Widget _myFindPage(String data) {
      page17kModel = Page17KModel.fromJson(json.decode(data));
      return Column(
        children: <Widget>[
          Container(
            height: 180.0,
            width: double.infinity,
            padding: EdgeInsets.only(top: 16.0),
            child: CachedNetworkImage(
              fit: BoxFit.fill,
              imageUrl: 'http://img.asj.com.cn/d/file/linggan/chahua/ch/108641-160923092050.jpg',
              placeholder: (BuildContext context, url) => CircularProgressIndicator(),
              errorWidget: (BuildContext context, url, error) => Icon(Icons.error),
            ),
          ),
          //推荐作品--product_like
          ProductLikeChip(bookList: page17kModel.productLike),
          //最新作品--product_new
          ProductNewChip(bookList: page17kModel.productNew),
          //热门作品--product_hot
          ProductHotChip(page17kModel: page17kModel),
        ],
      );
    }
}

