import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xinghai/page/main_pages/page_book/page_book_item.dart';

class MyCachedBookImage extends StatelessWidget {
  /// 图书链接
  final String bookImgUrl;
  /// 图书唯一标识
  final String bookIndex;
  /// 图书所在网站来源
  final String bookSourceId;
  /// 图书图片宽度，默认宽度为100.0
  final double width;
  /// 图书图片高度，默认高度为140.0
  final double height;

  MyCachedBookImage({
    this.bookImgUrl, this.bookIndex, this.bookSourceId,
    this.width = 100.0,  this.height = 140.0
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white70,
        boxShadow: [
          BoxShadow(offset: Offset(1.0, 1.0)),  //阴影
        ]
      ),
      child: Stack(
        children: <Widget>[
          Container(
            height: height,
            width: width,
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl: bookImgUrl,
              placeholder: (BuildContext context, url) => CircularProgressIndicator(),
              errorWidget: (BuildContext context, url, error) => Icon(Icons.error),
            ),
          ),
          //InkWell is good for display and tapping.
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor: Colors.white.withOpacity(0.5),
                highlightColor: Colors.white.withOpacity(0.1),
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => PageBookItem(bookIndex: bookIndex, bookSourceId: int.parse(bookSourceId),)
                  )
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}