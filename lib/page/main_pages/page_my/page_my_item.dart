import 'package:flutter/material.dart';
import 'package:flutter_xinghai/common/pages/my_error_page.dart';
import 'package:flutter_xinghai/common/pages/my_waiting_page.dart';
import 'package:flutter_xinghai/common/widgets/my_cached_book_image.dart';
import 'package:flutter_xinghai/constant/costant.dart';
import 'package:flutter_xinghai/entity/book.dart';
import 'package:flutter_xinghai/sqflite/book_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageMyItem extends StatefulWidget {
  @override
  _PageMyItemState createState() => _PageMyItemState();
}

class _PageMyItemState extends State<PageMyItem> {

  bool test =false;
  @override
  void initState() {
    super.initState();
  }

  

  Future<List<Book>> fetchData() async{
    final BookDataBase bookDataBase = BookDataBase();
    List<Book> bookList = await bookDataBase.getAllBook();
    return bookList;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MyWaitingPage();
        }
        List<Book> bookList = snapshot.data;
        if (bookList.isEmpty || bookList == null) {
          return MyErrorPage(
            title: '数据库出错',
            info: '查询浏览记录失败',
            method: () {
              setState(() {});
            },
            popContext: true,
          );
        }
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(58,95,131,1),
            title: Text('浏览记录',
              style: TextStyle(color: Colors.white),
            ),
            centerTitle: true,
          ),
          backgroundColor: Color.fromRGBO(58,95,131,1),
          body: ListView(
            children: List.generate(bookList.length, (index) {
              return getList(bookList[index]);
            }),
          ),
        );
      },
    );
  }
}

Widget getList(Book book) {
  return Container(
    padding: EdgeInsets.all(16.0),
    height: 200.0,
    color: Colors.blueGrey,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        MyCachedBookImage(
          bookImgUrl: book.bookImgUrl,
          bookIndex: book.bookIndex,
          bookSourceId: '${book.bookSourceId}',
          height: 154, width: 110
        ),
        Column(
          children: <Widget>[
            Text('${book.bookName}',
              style: TextStyle(color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold
              ),
            ),
            Container(
              width: 240.0,
              child: Text('${book.bookInfo}',
                  maxLines: 6,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.white,
                ),
              ),
            ),
          ],
        )
      ],
    ),
  );
}