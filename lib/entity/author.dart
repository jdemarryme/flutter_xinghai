import 'package:json_annotation/json_annotation.dart';
part 'author.g.dart';

@JsonSerializable()
class Author {
  int id;
  int authorId;
  String authorIndex;
  int authorSourceId;
  String authorImgUrl;
  String authorName;
  String authorBookIndexList;

  Author({
    this.id,
    this.authorSourceId,
    this.authorImgUrl,
    this.authorId,
    this.authorIndex,
    this.authorBookIndexList,
    this.authorName
  });

  factory Author.fromJson(Map<String, dynamic> json) => _$AuthorFromJson(json);
  Map<String, dynamic> toJson() => _$AuthorToJson(this);
}
