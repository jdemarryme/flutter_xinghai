import 'package:flutter/material.dart';



openSimpleDialog(BuildContext context, String title, List<Widget> widgets) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
        title: Text(title),
        children: widgets,
      );
    }
  );
}

/*
 * 打开提示框
 * @param context 上下文
 * @param content 用于提示的Widget类，如Text
 */
openAlertTipsDialog(BuildContext context, Widget content) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('提示'),
        content: content,
        actions: [
          FlatButton(
            child: Text('确定'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
  );
}



/* 
 * 已弃用（但是可用），弃用原因：界面元素不够友好
Widget getErrorPage(BuildContext context, JsonAndModel jsonAndModel) {
  return Scaffold(
    body: Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: RadialGradient(
              radius: 0.7,
              colors: [
                Colors.blueGrey,
                Colors.black87,
              ],
            )
          ),
        ),
        Center(child: Image.asset('assets/images/404.png', height: 450.0,)),
        Positioned(
          top: 30.0,
          right: 10.0,
          child: Image.asset('assets/images/title.png', color: Colors.grey[300],height: 40.0,),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 120.0,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text('${jsonAndModel.msg}', style: TextStyle(color: Colors.grey[400], fontSize: 22.0, fontWeight: FontWeight.bold),),
                FlatButton(
                  child: Image.asset('assets/images/return.png', color: Colors.grey[400],),
                    onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          ),
        )
      ],
    ),
  );
} */